INSTALL = install
INSTALL_DATA = ${INSTALL} -p -m 0644
INSTALL_BIN = ${INSTALL} -p -m 0755
SED = sed
PYTHON3 = /usr/bin/python3

prefix = /usr/local
datadir = ${prefix}/data
sbindir = ${prefix}/sbin
pkgdatadir = ${datadir}/ci-mirror
unitdir = ${prefix}/lib/systemd/system

SED_CMD = \
	-e 's!@pkgdatadir@!${pkgdatadir}!g' \
	-e 's!@mode@!$*!g' \
	-e 's!@PYTHON3@!${PYTHON3}!g' \

do_install = $1 -D '$(strip $2)' '${DESTDIR}$(strip $3)/$(notdir $(strip $2))'

bin_SCRIPTS = \
	bin/ci-mirror-runner \
	bin/ci-mirror-proxy \

contrib_UNITS = \
	contrib/ci-mirror-runner.service \
	contrib/ci-mirror-proxy.service \

all:	${bin_SCRIPTS} ${contrib_UNITS}

clean:
	rm -f ${bin_SCRIPTS} ${contrib_UNITS}

bin/ci-mirror-%:	bin/_ci-prog.in
	rm -f $@
	$(SED) ${SED_CMD} $< > $@
	@chmod a-w '$@'

contrib/ci-mirror-%.service:	contrib/_ci-mirror.service.in
	rm -f $@
	$(SED) ${SED_CMD} $< > $@
	@chmod a-w '$@'

install:	install-bin install-py install-units

install-bin:	${bin_SCRIPTS}
	$(call do_install,${INSTALL_BIN},bin/ci-mirror-runner,${sbindir})
	$(call do_install,${INSTALL_BIN},bin/ci-mirror-proxy, ${sbindir})

install-py:
	$(call do_install,${INSTALL_DATA},src/proxy.py,      ${pkgdatadir})
	$(call do_install,${INSTALL_DATA},src/runner.py,     ${pkgdatadir})
	$(call do_install,${INSTALL_DATA},src/gitlab_api.py, ${pkgdatadir})

install-units:	${contrib_UNITS}
	$(call do_install,${INSTALL_DATA},contrib/ci-mirror-runner.service,${unitdir})
	$(call do_install,${INSTALL_DATA},contrib/ci-mirror-proxy.service, ${unitdir})
