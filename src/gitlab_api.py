# Copyright (C) 2018 Enrico Scholz <enrico.scholz@sigma-chemnitz.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import requests
import pycurl
from urllib.parse import urlencode

def filter_args(args):
    return dict(filter(lambda x: x[1] is not None, args.items()))

def encode_json(args):
    args = filter_args(args)
    return json.dumps(args)

def encode_http(args, prefix=""):
    res = {}
    for (k,v) in args.items():
        if v is None:
            continue

        if isinstance(v, list):
            res["%s%s[]" % (prefix, k)] = v
        elif isinstance(v, dict):
            res.update(encode_http(v, "%s[%s]" % (prefix, k)))
        else:
            res["%s%s" % (prefix, k)] = v

    return dict(res)

class CurlResponse:
    def __init__(self, c):
        self.status_code = c.getinfo(c.RESPONSE_CODE)

    def raise_for_status(self):
        if self.status_code < 200 or self.status_code > 299:
            raise Exception("HTTP failed with %u" % self.status_code)

## legacy version support
class ResponseStreamWrap:
    def __init__(self, stream):
        self.__stream = stream

    def __enter__(self):
        return self.__stream

    def __exit__(self, type, value, tb):
        self.__stream.close()


class AuthCtx:
    def __init__(self):
        pass

    @staticmethod
    def _update_kwargs(kwargs, field, info):
        data = (kwargs.get(field) or {}).copy()
        data.update(info)
        kwargs[field] = data

class OauthCtx(AuthCtx):
    def __init__(self, token):
        self.__token = token

    def update_args(self, kwargs):
        info = { 'PRIVATE-TOKEN' : self.__token}
        self._update_kwargs(kwargs, 'headers', info)

class JobAuthCtx(AuthCtx):
    def __init__(self, token):
        self.__token = token

    def update_args(self, kwargs):
        # TODO: this is ugly... some API calls require the token in
        # the http data, some in the header.  Write it into both
        # places for now
        self._update_kwargs(kwargs, 'headers', {
            "JOB-TOKEN" : self.__token
        })
        self._update_kwargs(kwargs, 'data', {
            "token" : self.__token
        })

class RunnerAuthCtx(AuthCtx):
    def __init__(self, token):
        self.__token = token

    def update_args(self, kwargs):
        info = { 'token' : self.__token}
        self._update_kwargs(kwargs, 'data', info)

class Gitlab:
    def __init__(self, server, auth = None):
        self.__server = server
        self.__auth = auth

    def reauth(self, auth):
        return Gitlab(self.__server, auth)

    def __run_kwargs(self, method, api, kwargs):
        uri = self.__server + api

        if self.__auth:
            self.__auth.update_args(kwargs)

        if 'data_raw' in kwargs:
            data = kwargs['data_raw']
            del kwargs['data_raw']
            kwargs['data'] = data
        elif 'data' in kwargs:
            data = kwargs['data']
            del kwargs['data']
            kwargs['data'] = encode_http(data)

        #print("\n=======================\n%s\n=====================" % kwargs)
        return method(uri, **kwargs)

    def post(self, api, **kwargs):
        return self.__run_kwargs(requests.post, api, kwargs)

    def post_curl(self, api, **kwargs):
        uri = self.__server + api

        if self.__auth:
            self.__auth.update_args(kwargs)

        c = pycurl.Curl()
        c.setopt(c.URL, uri)

        hdrs = []
        finfo = []
        if 'data' in kwargs:
            data = kwargs['data']
            data = encode_http(data)

            for k,v in data.items():
                if type(v) == int:
                    v = str(v)
                finfo.append((k, v))

        for k,v in kwargs.get('headers', {}).items():
            hdrs.append("%s: %s" % (k, v))

        for f in kwargs.get('files', ()):
            ## TODO: use CURLFORM_STREAM?
            finfo.append((f[0], (
                c.FORM_FILE, f[1][1].name,
                c.FORM_FILENAME, f[1][0],
                c.FORM_CONTENTTYPE, f[1][2],
            )))

        #print("\n=======================\n%s\n=====================" % finfo)

        c.setopt(c.HTTPHEADER, hdrs)
        c.setopt(c.HTTPPOST, finfo)
        c.perform()
        res = CurlResponse(c)
        c.close()

        return res

    def put(self, api, **kwargs):
        return self.__run_kwargs(requests.put, api, kwargs)

    def get(self, api, **kwargs):
        return self.__run_kwargs(requests.get, api, kwargs)

    def patch(self, api, **kwargs):
        return self.__run_kwargs(requests.patch, api, kwargs)

class Option:
    @staticmethod
    def none(template):
        return None

class Runner:
    VERSION = {
        'name' : 'gitlab-ci-mirror',
        'version' : '0.0.0',
        'features' : {
            'masking' : True,
            'raw_variables' : True,
            'refspecs' : True,
            'artifacts' : True,
            'artifacts_exclude' : False,
            'cache' : True,
            'upload_multiple_artifacts' : True,
            'upload_raw_artifacts' : True,
            'variables' : True,
            'image' : True,
            'services' : False,
            'shared' : True,
            'session' : False,
            'terminal' : False,
            'proxy' : False,
        }
    }

    def __init__(self, gitlab, token):
        auth = RunnerAuthCtx(token)
        self.__gitlab = gitlab.reauth(auth)

    @staticmethod
    def register(gitlab, token, overrides = {}):
        args = {
            "description"     : Option.none(""),
            "locked"          : Option.none(False),
            "run_untagged"    : Option.none(True),
            "tag_list"        : Option.none([]),
            "maximum_timeout" : Option.none(1),
        }

        args.update(overrides)

        gitlab = gitlab.reauth(RunnerAuthCtx(token))

        r = gitlab.post("/runners", data = args)
        r.raise_for_status()

        token = r.json()["token"]
        return (Runner(gitlab, token), token)

    def job_request(self):
        args = {
            "last_update" : Option.none(""),
            "info" : self.VERSION,
        }

        r = self.__gitlab.post("/jobs/request", data = args)
        r.raise_for_status()

        if r.status_code == 204:
            # no job available
            return None
        elif r.status_code == 201:
            return RunnerJob(self.__gitlab, r.json())

class RunnerJob:
    def __init__(self, gitlab, info):
        auth = JobAuthCtx(info['token'])
        self.__gitlab = gitlab.reauth(auth)
        self.__id = info['id']
        self.__trace_pos = 0
        self.info = info

    def __update(self, overrides):
        args = {
            "id" : self.__id,
            "expire_in" : Option.none(""),
        }
        args.update(overrides)

        # see lib/api/ci/runner.rb
        # requires :ci_trace_overwrite for now
        r = self.__gitlab.put("/jobs/%u" % self.__id, data = args)
        r.raise_for_status()

    def send_trace(self, trace):
        # when trace is empty we can not calculate "Content-Range";
        # return without sending anything
        if trace is None or len(trace) == 0:
            return

        headers = {
            "Content-Range": "0-%u" % (len(trace)-1,),
        };

        r = self.__gitlab.patch("/jobs/%u/trace" % self.__id,
                                data_raw = trace,
                                headers = headers)
        r.raise_for_status()

    def abort(self, msg = "aborted"):
        self.__update({'trace' : msg,
                       'state' : 'failed'})

    def finish(self):
        self.__update({'state' : 'success'})

    def upload_artifact(self, fname, fd, expire = None, sz = None):
        args = {
            "id" : self.__id,
            "expire_in" : expire,
        }

        files = {
            ("file", (fname, fd, "application/octet-stream", sz))
        }

        r = self.__gitlab.post_curl("/jobs/%u/artifacts" % self.__id,
                                    data = args, files = files)

        if r.status_code == 413:
            return False

        r.raise_for_status()

        return True

class Project:
    def __init__(self, gitlab, project_id):
        self.__gitlab = gitlab
        self.__id = project_id

    def api_path(self, extra):
        return "/projects/%u%s" % (self.__id, extra)

    def get_id(self):
        return self.__id

    def pipelines(self, params):
        args = {
            'id' : self.__id,
            'scope' : Option.none(""),
            'status' : Option.none(""),
            'ref' : Option.none(""),
            'sha' : Option.none(""),
            "yaml_errors" : Option.none(False),
            "name" : Option.none(""),
            "username" : Option.none(""),
            "order_by" : Option.none(""),
            "sort" : Option.none(""),
        }
        args.update(params)

        r = self.__gitlab.get(self.api_path("/pipelines"), data = args)
        r.raise_for_status()

        info = r.json()
        if 'sha' in params:
            sha = params['sha']
            info = filter(lambda x: x['sha'] == sha, info)

        return map(lambda x: Pipeline.from_list(self.__gitlab, self, x), info)

class Pipeline:
    def __init__(self, gitlab, pipeline_id):
        self.__gitlab = gitlab
        self.__id = pipeline_id
        self.__project = None

    def api_path(self, extra):
        return self.__project.api_path("/pipelines/%u%s" % (self.__id, extra))

    def get_project(self):
        return self.__project

    def jobs(self):
        args = {
            "scope" : "success"
        }

        r = self.__gitlab.get(self.api_path("/jobs"), data = args);
        r.raise_for_status()

        return map(lambda x: Job.from_list(self.__gitlab, self, x),
                   r.json())

    def get_status(self):
        return self.__status

    @staticmethod
    def from_list(gitlab, project, info):
        res = Pipeline(gitlab, info['id'])
        res.__status = info['status']
        res.__project = project

        return res

class Job:
    def __init__(self, gitlab, job_id):
        self.__id = job_id
        self.__gitlab = gitlab
        self.__project = None

    def api_path(self, extra):
        return self.__project.api_path("/jobs/%u%s" % (self.__id, extra))

    @staticmethod
    def from_list(gitlab, pipeline, info):
        res = Job(gitlab, info['id'])
        res.__project = pipeline.get_project()
        res.info = info

        return res

    def __get_stream(self, path):
        r = self.__gitlab.get(self.api_path(path), stream = True)
        r.raise_for_status()

        clen = r.headers.get("Content-Length")
        if clen is not None:
            clen = int(clen)

        ## legacy version support
        if not hasattr(r, '__enter__'):
            r = ResponseStreamWrap(r)

        return (r, clen)

    def get_trace(self):
        return self.__get_stream("/trace")

    def get_artifact(self):
        return self.__get_stream("/artifacts")
