#! /usr/bin/python3

# Copyright (C) 2018 Enrico Scholz <enrico.scholz@sigma-chemnitz.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import sys
import configparser
import shutil
import logging

if sys.version_info < (3,0):
    import BaseHTTPServer as http_server
    import httplib as http_client
    import SocketServer as socketserver
else:
    import http.client as http_client
    import http.server as http_server
    import socketserver

from gitlab_api import *

#http_client.HTTPConnection.debuglevel = 1

CFG = configparser.ConfigParser()
CFG.read_dict({ "proxy" : {
    'listen' : '::',
    'port' : 8001
}})

logger = None

def init_logging(is_debug):
    global logger

    logging.basicConfig()

    logger = logging.getLogger('ci-proxy')
    logger.setLevel(logging.INFO)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    http_log = logging.getLogger("requests.packages.urllib3")
    http_log.propagate = True

    if is_debug:
        logging.getLogger().setLevel(logging.DEBUG)
        http_client.HTTPConnection.debuglevel = 1
        http_log.setLevel(logging.DEBUG)

    logger.addHandler(ch)

class HttpHandler(http_server.BaseHTTPRequestHandler):
    MAX_REQ_SIZE = 1000000

    def do_POST(self):
        clen = self.headers.get('Content-Length')
        if clen is None:
            self.send_error(411)

        clen = int(clen)

        if clen > self.MAX_REQ_SIZE:
            self.send_error(413)

        try:
            req = json.loads(self.rfile.read(clen))
        except:
            self.send_error(422)

        job = self.__find_job(req)
        if not job:
            # __find_job() emits http error already
            pass
        elif self.path.endswith("/trace"):
            return self.__run_trace(job, req)
        elif self.path.endswith("/artifact"):
            return self.__run_artifact(job, req)
        elif self.path.endswith("/info"):
            return self.__run_info(job, req)
        else:
            self.send_error(400, "bad request")

    def __find_job(self, info):
        global CFG

        auth = OauthCtx(info['token'])
        gitlab = Gitlab(CFG['proxy']['server'], auth)

        project = Project(gitlab, info['project'])
        pipelines = project.pipelines({
            'ref'    : info['ref'],
            'sha'    : info['sha'],
        })

        jobs = []
        for p in pipelines:
            if p.get_status() in ['running', 'pending']:
                self.send_error(424, "pipeline still running")
                return None

            if p.get_status() in ['success']:
                jobs.extend(filter(lambda x: (x.info['stage'] == info['stage'] and
                                              x.info['name']  == info['job'] and
                                              x.info['name']  != 'pages:deploy'),
                                   p.jobs()))

        if len(jobs) == 0:
            self.send_error(404, "No such pipeline")
            return None
        elif len(jobs) > 1:
            return jobs[-1]
            self.send_error(406, "Too much matching jobs")
            return None
        else:
            return jobs[0]

    def __run_trace(self, job, req):
        try:
            (trace_stream, sz) = job.get_trace()
        except:
            self.send_response(500, "internal error while fetching traces")
            self.end_headers()
            raise

        with trace_stream as data:
            self.send_response(200)
            if sz is not None:
                self.send_header("Content-Length", sz)
            self.end_headers()

            for d in data.iter_content(512 * 1024):
                self.wfile.write(d)

    def __run_info(self, job, req):
        info = {
            'artifact' : job.info.get('artifacts_file'),
            'artifacts': job.info.get('artifacts'),
            'started'  : job.info['started_at'],
            'finished' : job.info['finished_at'],
            'created'  : job.info['created_at'],
            'runner'   : job.info['runner'],
            'coverage' : job.info['coverage'],
        };

        if sys.version_info < (3,0):
            info = json.dumps(info)
        else:
            info = bytes(json.dumps(info), "us-ascii")

        self.send_response(200);
        self.send_header('Content-Length', len(info))
        self.end_headers()
        self.wfile.write(info)

    def __run_artifact(self, job, req):
        try:
            (artifact_stream, sz) = job.get_artifact()
        except:
            self.send_response(500, "internal error while fetching artifact")
            self.end_headers()
            raise

        with artifact_stream as data:
            self.send_response(200)
            if sz is not None:
                self.send_header("Content-Length", sz)
            self.end_headers()

            for d in data.iter_content(512 * 1024):
                self.wfile.write(d)

class CompatTCPServer(socketserver.TCPServer):
    def __enter__(self):
        return self

    def __exit__(self, type, value, tb):
        self.server_close()

TCPServer = CompatTCPServer

class TCP6Server(TCPServer):
    import socket

    address_family = socket.AF_INET6

def main():
    import argparse

    init_logging(False)

    parser = argparse.ArgumentParser(description='gitlab ci mirror runner')
    parser.add_argument("--cfgfile", "-c", metavar="FILE", type=str,
                        help="configuration file",
                        default = "/etc/ci-mirror.conf")
    parser.add_argument("--debug", action = 'store_const', const = 'debug',
                        default = False)

    args = parser.parse_args()
    if args.debug:
        http_client.HTTPConnection.debuglevel = 1

    CFG.read(args.cfgfile)

    host = CFG['proxy']['listen']
    port = int(CFG['proxy']['port'])

    if CFG['proxy'].get('is_ipv4', False):
        server = TCPServer
    else:
        server = TCP6Server

    with server((host, port), HttpHandler, False) as httpd:
        import socket
        httpd.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        httpd.server_bind()
        httpd.server_activate()
        httpd.serve_forever()

if __name__ == '__main__':
    main()
