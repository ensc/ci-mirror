#! /usr/bin/python3

# Copyright (C) 2018 Enrico Scholz <enrico.scholz@sigma-chemnitz.de>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; version 3 of the License.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import tempfile
import io
import time
import sys
import configparser
import requests
import logging
import gc

from multiprocessing import Process

if sys.version_info < (3,0):
    import httplib as http_client
else:
    import http.client as http_client

CFG = configparser.ConfigParser()
CFG.read_dict({ "proxies" : {},
                "runner" : {}, })

from gitlab_api import *

logger = None

def init_logging(is_debug):
    global logger

    logging.basicConfig()

    logger = logging.getLogger('ci-runner')
    logger.setLevel(logging.INFO)

    ch = logging.StreamHandler()
    ch.setLevel(logging.DEBUG)

    http_log = logging.getLogger("requests.packages.urllib3")
    http_log.propagate = True

    if is_debug:
        logging.getLogger().setLevel(logging.DEBUG)
        http_client.HTTPConnection.debuglevel = 1
        http_log.setLevel(logging.DEBUG)

    logger.addHandler(ch)

class ConfigurationError(Exception):
    def __init__(self, msg):
        self.__msg = msg

    def __str__(self):
        return self.__msg

class MissingVariable(ConfigurationError):
    def __init__(self, varname):
        self.__varname = varname
        ConfigurationError.__init__(self, "Missing variable '%s'" % self.__varname)

class Job:
    def __init__(self, job):
        self.__job = job
        self.__expire = None
        self.__proxy = None
        self.__info = {
            'sha' : job.info['git_info']['sha'],
            "ref" : job.info['git_info']['ref'],
            "job" : job.info['job_info']['name'],
            "stage"   : job.info['job_info']['stage'],
        }

        logger.info("Running job '%s' (%s)" % (self.__info['job'], self.__info['sha']))


    def init(self):
        job = self.__job

        self.__info.update(
            {
                "token"   : self.get_job_variable("CI_MIRROR_TOKEN", job, True),
                "project" : int(self.get_job_variable("CI_MIRROR_SOURCE_ID", job, True)),
            }
        )

        artifacts = job.info.get('artifacts')
        if artifacts:
            self.__expire = artifacts[0].get("expire_in")

        jobname = self.canonify_varname(self.__info['job'])
        expire = self.get_job_variable("CI_MIRROR_EXPIRE_%s" % jobname, job)
        if expire:
            self.__expire = expire

        source_host = self.get_job_variable("CI_MIRROR_SOURCE_HOST", job)
        self.__proxy = self.find_proxy(source_host)

    @staticmethod
    def canonify_varname(name):
        res = ""
        for c in name[:]:
            if c == '_':
                res += '__'
            elif not c.isalnum():
                res += '_'
            else:
                res += c

        return res

    @staticmethod
    def find_proxy(host):
        global CFG

        uri = None
        if host:
            uri = CFG['proxies'].get(host)
        if not uri:
            uri = CFG['proxies'].get('_default_')

        if not uri:
            raise ConfigurationError("failed to find proxy for '%s'" % host)

        return uri

    @staticmethod
    def get_job_variable(name, job, required = False):
        for var in job.info['variables']:
            #print("VARS: %s" % var)
            if var['key'] == name:
                return var['value']

        if required:
            raise MissingVariable(name)

        return None

    def boiler_plate(self, remote_info):
        return """\
Mirrored by gitlab-ci-mirror; original information:

  Created:  %(tm_created)s
  Started:  %(tm_started)s
  Finished: %(tm_finished)s (%(tm_delta)s)

========================================================

""" % remote_info

    def run(self):
        while True:
            r = requests.post(self.__proxy + "/info", json = self.__info)
            if r.status_code == 424:
                logger.info("job not ready yet")
                delay = int(CFG['runner'].get('delay_pending', 10))
                time.sleep(delay)
                continue

            break

        r.raise_for_status()
        job_info = r.json()

        r = requests.post(self.__proxy + "/trace", json = self.__info)
        r.raise_for_status()

        trace = self.boiler_plate({
            'tm_created' : job_info['created'],
            'tm_started' : job_info['started'],
            'tm_finished' : job_info['finished'],
            'tm_delta' : "???"
        }) + r.text

        self.__job.send_trace(trace.encode())

        trace = None
        r = None

        gc.collect()

        artifact_info = job_info.get('artifact')
        if artifact_info:
            with tempfile.NamedTemporaryFile("w+b") as artifact:
                r = requests.post(self.__proxy + "/artifact",
                                  json = self.__info, stream = True)
                r.raise_for_status()

                ## legacy version support
                if not hasattr(r, '__enter__'):
                    r = ResponseStreamWrap(r)

                with r as data:
                    for buf in data.iter_content(512 * 1024):
                        artifact.write(buf)

                artifact.flush()
                sz = artifact.seek(0, io.SEEK_CUR)
                artifact.seek(0, io.SEEK_SET)

                res = self.__job.upload_artifact(artifact_info['filename'], artifact,
                                                 self.__expire, sz)

                if not res:
                    logger.warn("Skiiping artificat upload because rejected upstream")

        self.__job.finish()

    def abort(self, msg = None):
        return self.__job.abort(msg)

def run_job(job):
    job = Job(job)
    try:
        job.init()
        job.run()
    except ConfigurationError as e:
        logger.error("CFG ERROR: %s" % e)
        job.abort("Error in mirror configuration:\n  %s\n" % e)
    except Exception as e:
        logger.exception("ERROR: %s" % e)
        job.abort()

def run(register_token = None, cfg_file = None, one_shot = False):
    global CFG

    gitlab = Gitlab(CFG['runner']['uri'])
    runner_auth = CFG['runner'].get('token')

    if runner_auth:
        runner = Runner(gitlab, runner_auth)
    elif register_token:
        overrides = {
            'description'     : "mirror-only ci server; no real builds",
            'tag_list'        : 'CI-MIRROR',
            #'locked'         : False,
            'maximum_timeout' : 4 * 60 * 60,
        }
        (runner, token) = Runner.register(gitlab, register_token,
                                          overrides = overrides)
        CFG['runner']['token'] = token
        if cfg_file:
            with open(cfg_file, "w") as cfg:
                CFG.write(cfg)
    else:
        raise Exception("missing token")

    while True:
        try:
            job = runner.job_request()
        except Exception as e:
            logger.exception("ERROR:failed to request job: %s" % e)
            job = None

        if not job:
            delay = int(CFG['runner'].get('delay_idle', 30))
        else:
            p = Process(target = run_job, args = (job,))
            p.start()
            p.join()

            delay = int(CFG['runner'].get('delay_run', 1))

        if one_shot:
            break

        gc.collect()
        time.sleep(delay)

def main():
    import argparse

    init_logging(False)

    parser = argparse.ArgumentParser(description='gitlab ci mirror runner')
    parser.add_argument("--cfgfile", "-c", metavar="FILE", type=str,
                        help="configuration file",
                        default = "/etc/ci-mirror.conf")
    parser.add_argument("--debug", action = 'store_const', const = 'debug',
                        default = False)
    parser.add_argument("--oneshot", action = 'store_const', const = 'oneshot',
                        default = False)
    parser.add_argument("--token", type = str, metavar = "TOKEN",
                        help = "runner registration token",
                        default = None)

    args = parser.parse_args()
    if args.debug:
        http_client.HTTPConnection.debuglevel = 1

    CFG.read(args.cfgfile)

    if args.token:
        register_token = args.token
    else:
        register_token = CFG['runner'].get('register_token')

    run(register_token = register_token,
        cfg_file = args.cfgfile,
        one_shot = args.oneshot)

if __name__ == '__main__':
    if sys.version_info >= (3,0):
        import multiprocessing
        multiprocessing.set_start_method('fork', True)

    main()
